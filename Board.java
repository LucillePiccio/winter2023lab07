public class Board{
	private Square[][] tictactoeBoard;
	
	//constructor
	public Board(){
		Square blank = Square.BLANK;
		this.tictactoeBoard = new Square[3][3];
		for(int i = 0; i < this.tictactoeBoard.length; i++){
			for(int j = 0; j < this.tictactoeBoard.length; j++){
				this.tictactoeBoard[i][j] = blank;
			}
		}
	}
	//instance methods
	public String toString(){
		String setup = "";
		for(int i = 0; i < this.tictactoeBoard.length; i++){
			for(int j = 0; j < this.tictactoeBoard.length; j++){
				System.out.print(this.tictactoeBoard[i][j] + " ");
			}
			System.out.println();
		}
		return setup;
	}
	public boolean placeToken(int row, int col, Square playerToken){
		boolean isValid = false;
		//0, 1, 2 (equal to 3 for an array)
		if((row >= 0 && row <=2) && (col >= 0 && col <=2)){
				isValid = true;
		}
		//no else because isValid would return to its default value (false)
		if(this.tictactoeBoard[row][col] == Square.BLANK){
			this.tictactoeBoard[row][col] = playerToken;
			isValid = true;
			//no else because isValid would return to its default value (false)
		}
		return isValid;
	}
	public boolean checkIfFull(){
		Square blank = Square.BLANK;
		boolean isBlank = true;
		//going through the board for any BLANK values using a for-each loop
		for(Square[] area: tictactoeBoard){
			//i refering to area which is the game board
			for(Square i: area){
				//comparing the board for a blank value
				if(i == blank){
					isBlank = false;
				}
			}
		}
		return isBlank;
	}
	//check if a player completed a horizontal line
	//only check the outer loop for row
	private boolean checkIfWinningHorizontal(Square playerToken){
		boolean isRowTaken = false;
		
		for(int i = 0; i < this.tictactoeBoard.length; i++){
			if(this.tictactoeBoard[i][0] == playerToken && this.tictactoeBoard[i][1] == playerToken 
			&& this.tictactoeBoard[i][this.tictactoeBoard.length - 1] == playerToken){
				isRowTaken = true;
			}
		}
		return isRowTaken;
	}
	//check if a player completed a vertical line
	//only checks the inner look for column
	private boolean checkIfWinningVertical(Square playerToken){
		boolean isColTaken = false;
		for(int i = 0; i < this.tictactoeBoard.length; i++){
			if(this.tictactoeBoard[0][i] == playerToken && this.tictactoeBoard[1][i] == playerToken
			&& this.tictactoeBoard[this.tictactoeBoard.length - 1][i] == playerToken){
				isColTaken = true;
			}
		}
		return isColTaken;		
	}
	//checks if a player has one the game
	public boolean checkIfWinning(Square playerToken){
		boolean isGameOver = false;
		if(checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken)){
			isGameOver = true;
		}
		return isGameOver;
	}
}
