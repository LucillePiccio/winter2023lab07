import java.util.Scanner;
public class TicTacToe {
	public static void main(String[]args){
		//testing Square.java
		//Square s = Square.BLANK;
		//System.out.println(s.BLANK);
		
		//testing tictactoe for representation of the board
		//Board b = new Board();
		//System.out.println(b.toString());
		
		//creating board object
		Scanner scan = new Scanner(System.in);
		Board b = new Board();
		System.out.println("Welcome to the Winter 2023 TicTacToe games!");
		
		//creating needed variables
		boolean gameOver = false;
		int player = 1;
		Square playerToken = Square.X;
		
		//creating game loop
		while(!(gameOver)){
		System.out.println(b.toString());
		if(player == 1){
			playerToken = Square.X;
		}
		else{
			playerToken = Square.O;
		}
	System.out.println("Player " + player + "'s turn");
	System.out.println("Which row would you like to place your token : 0, 1 or 2");
	int row = scan.nextInt();
	System.out.println("Which column would you like to place your token : 0, 1 or 2");
	int col = scan.nextInt();
	
	//calling placeToken method from board class
	if(!(b.placeToken(row, col, playerToken))){
		System.out.println("Entered area is invalid, please enter a different area");
		System.out.println("Player " + player + " Enter row: 0, 1 or 2");
		System.out.println("Player " + player + " Enter row: 0, 1 or 2");
		row = scan.nextInt();
		System.out.println("Player " + player + " Enter column: 0, 1 or 2");
		col = scan.nextInt();
	}
	//when token is succesfully placed
	if(b.checkIfWinning(playerToken)){
		System.out.println(b.toString());
		System.out.println("Player " + player + " has won the game, Congratulations!");
		gameOver = true;
	}
	//if no more areas can accept a token
	else if(b.checkIfFull()){
		System.out.println(b.toString());
		System.out.println("It's a tie!");
		gameOver = true;
	}
	else{
		player++;
			if(player > 2){
				player = 1;
				}	
			}
		}
	}
}
